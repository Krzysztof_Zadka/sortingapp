# Sorting App Project

This exercise is about getting familiar with Apache Maven and JUnit. It's simple sorting program that uses bubble sort. There are provided unit tests, some of them are parametrized. Project is also configured to build a runnable jar.