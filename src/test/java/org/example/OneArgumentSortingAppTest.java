package org.example;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.*;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class OneArgumentSortingAppTest {

    protected Sorter sorter = new Sorter();

    private List<Integer> integers;
    private String expected;

    public OneArgumentSortingAppTest(List<Integer> integers, String expected) {
        this.integers = integers;
        this.expected = expected;
    }

    @Test
    public void oneArgumentCase() {
        String actual = sorter.sort(integers);

        assertEquals(expected, actual);
    }

    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {Collections.singletonList(5), "5"},
                {Collections.singletonList(-167), "-167"},
                {Collections.singletonList(0), "0"},
                {Collections.singletonList(Integer.MAX_VALUE), String.valueOf(Integer.MAX_VALUE)},
                {Collections.singletonList(Integer.MIN_VALUE), String.valueOf(Integer.MIN_VALUE)}
        });
    }

}
