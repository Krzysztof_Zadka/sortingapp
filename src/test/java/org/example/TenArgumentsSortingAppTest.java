package org.example;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class TenArgumentsSortingAppTest {

    protected Sorter sorter = new Sorter();

    private List<Integer> integers;
    private String expected;

    public TenArgumentsSortingAppTest(List<Integer> integers, String expected) {
        this.integers = integers;
        this.expected = expected;
    }

    @Test
    public void tenArgumentCase() {
        String actual = sorter.sort(integers);

        assertEquals(expected, actual);
    }

    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10), "1, 2, 3, 4, 5, 6, 7, 8, 9, 10"},
                {Arrays.asList(-1105, -1453, 1013, 236,	2477, 683, -1113, 1516,	2, 1616),
                "-1453, -1113, -1105, 2, 236, 683, 1013, 1516, 1616, 2477"},
                {Arrays.asList(1444, 845, 677, 765, 1478, 1864, 1488, 1253, 528, 756),
                "528, 677, 756, 765, 845, 1253, 1444, 1478, 1488, 1864"},
                {Arrays.asList(477, 212, -223, 363, 570, 112, -296, 192, 666, 653),
                "-296, -223, 112, 192, 212, 363, 477, 570, 653, 666"},
                {Arrays.asList(-282, -21, -189, -270, 93, -30, -496, -126, -325, 68),
                "-496, -325, -282, -270, -189, -126, -30, -21, 68, 93"}
        });
    }

}
