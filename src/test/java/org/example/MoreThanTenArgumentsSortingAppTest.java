package org.example;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class MoreThanTenArgumentsSortingAppTest {
    
    protected Sorter sorter = new Sorter();

    @Test
    public void moreThanTenArgumentCase() {
        String expected = "To many arguments!";
        List<Integer> toManyIntegers = Arrays.asList(201, -159, 1103, 1909, 1452, 896, - 694, -669, -761, 1414, -374);

        String actual = sorter.sort(toManyIntegers);

        assertEquals(expected, actual);
    }
}
