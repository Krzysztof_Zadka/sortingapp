package org.example;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ZeroArgumentsSortingAppTest {

    protected Sorter sorter = new Sorter();


    @Test
    public void testZeroArgumentsCase() {
        String expected = "No arguments!";
        List<Integer> noIntegers = new ArrayList<>();

        String actual = sorter.sort(noIntegers);

        assertEquals(expected, actual);
    }

}
