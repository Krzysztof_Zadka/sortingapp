package org.example;

import java.util.List;

public class Sorter {
    public String sort(List<Integer> list) {

        if (list.isEmpty()) {
            return "No arguments!";
        }

        if (list.size() > 10) {
            return "To many arguments!";
        }
        int temp;

        for (int i = 0; i < list.size(); i++) {
            for (int j = i + 1; j < list.size(); j++) {
                if (list.get(i) > list.get(j)) {
                    temp = list.get(i);
                    list.set(i, list.get(j));
                    list.set(j, temp);
                }
            }
        }
        return list.toString().replace("[", "").replace("]", "");
    }

}
