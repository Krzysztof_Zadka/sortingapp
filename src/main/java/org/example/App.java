package org.example;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * This is simple app that uses bubble sort algorithm to sort specified list of integers.
 * There can be up to 10 numbers to sort. Program reads input line by line as a string
 * and then parses it to list of ints. The return value is string with numbers sorted in ascending order.
 * After reading an empty line and returning the result of sort program is closed.
 *
 * There are provided unit tests, the ones with one and ten arguments are parametrized.
 */
public class App {
    public static void main( String[] args ) {

        Scanner scanner = new Scanner(System.in);
        Sorter sorter = new Sorter();

        System.out.println("Hello, provide up to 10 numbers that you want to sort. One by one please.");

        List<Integer> numbers = new ArrayList<>();
        String line;

        while (!(line = scanner.nextLine()).isEmpty()) {
            try {
                numbers.add(Integer.valueOf(line));
            } catch (NumberFormatException e) {
                System.out.println("Not a number.");
            }
        }

        System.out.println(sorter.sort(numbers));

    }
}
